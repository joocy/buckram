#/bin/sh

COMMAND=$(printf '        - %s\\\\n' "$@")'#'

NAME=laravel-job-$(date +%s)
RELEASE=$(helm list | grep buckram | awk '{print $1}')

echo "
apiVersion: batch/v1
kind: Job
metadata:
  name: ${NAME}
spec:
  template:
$( \
  kubectl get cronjob.v2alpha1.batch ${RELEASE}-laravel-artisan -o yaml | \
  grep '^        ' | \
  sed \
    -e 's/^    //' \
    -e "s/creationTimestamp: null/name: ${NAME}/" \
    -e "s/laravel-artisan-scheduler/${NAME}/g" \
    -e "s/\s*- schedule:run/$COMMAND/" \
)" | kubectl create -f -

POD="$(kubectl get pods -a | grep ${NAME} | awk '{printf $1}')"
echo "Found pod: ${POD}"

until kubectl logs -f "${POD}"
do
  sleep 1
done
