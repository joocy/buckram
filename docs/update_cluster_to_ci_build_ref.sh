#!/bin/bash

# This is a placeholder script until RBAC with CI is finalized, at which time it
# will be removed.

if [ $# -ne 2 ]
then
    echo "Usage: ./update_cluster_to_ci_build_ref.sh KUBECTL_FROM_CONTEXT KUBECTL_TO_CONTEXT"
    echo "NOTE: this script is a stand-in for RBAC-based functionality and should be used with care"
    exit
fi

FROM_CONTEXT="$1"
TO_CONTEXT="$2"

TO_NAME=$(helm --kube-context=${TO_CONTEXT} list | grep buckram | awk '{print $1}')
FROM_NAME=$(helm --kube-context=${FROM_CONTEXT} list | grep buckram | awk '{print $1}')
echo "Updating release ${TO_NAME} from ${FROM_NAME}"

TO_PHPFPM_CONTAINER=$(kubectl get deployment $TO_NAME-laravel-phpfpm --context=${TO_CONTEXT} -o=custom-columns='NAME:.spec.template.spec.containers[0].image' --no-headers)
FROM_PHPFPM_CONTAINER=$(kubectl get deployment $FROM_NAME-laravel-phpfpm --context=${FROM_CONTEXT} -o=custom-columns='NAME:.spec.template.spec.containers[0].image' --no-headers)

TO_REPO=$(echo $TO_PHPFPM_CONTAINER | sed 's/:.*//g')
FROM_REVISION=$(echo $FROM_PHPFPM_CONTAINER | sed 's/.*:.*-//g')

echo "Basing ${TO_NAME} update off the PHP-FPM deployment, with repo ${TO_REPO} and revision ${FROM_REVISION}"

select answer in "Proceed" "Cancel"; do
    case $answer in
	Proceed )
	    echo "Yes"

	    kubectl --context=${TO_CONTEXT} set image deployment/$TO_NAME-laravel-nginx nginx="$TO_REPO:nginx-${FROM_REVISION}"
	    kubectl patch cronjob.v2alpha1.batch $TO_NAME-laravel-artisan -p "{\"spec\": {\"jobTemplate\": {\"spec\": {\"template\": {\"spec\": {\"containers\": [{\"name\": \"laravel-artisan-scheduler\",\"image\": \"$TO_REPO:artisan-${FROM_REVISION}\"}]}}}}}}"
	    kubectl --context=${TO_CONTEXT} set image deployment/$TO_NAME-laravel-artisan laravel-artisan-worker="$TO_REPO:artisan-${FROM_REVISION}"
	    kubectl --context=${TO_CONTEXT} set image deployment/$TO_NAME-laravel-phpfpm laravel-phpfpm="$TO_REPO:phpfpm-${FROM_REVISION}"

	    break
	    ;;
	Cancel )
	    echo "No"
	    exit
	    ;;
    esac
done
