#!/bin/bash

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

cp $SCRIPT_DIR/config/secrets/laravel.env.local $SCRIPT_DIR/config/secrets/laravel.env
cp $SCRIPT_DIR/config/secrets/redis.env.local $SCRIPT_DIR/config/secrets/redis.env
