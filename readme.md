# Buckram

## A Laravel Chart

Caveat: this structure is still in active development.

To use this with Laravel:

- clone/copy this repository into a new `infrastructure` subdirectory of your Laravel project
- to use with Gitlab, move `gitlab-ci.yml` to `.gitlab-ci.yml` in your root directory (note leading dot)
- add an `APP_KEY` entry to your `phpunit.xml` variables to ensure a sample entry is available during test

### Laravel with GitlabCI+Kubernetes extensions

For details of the upstream Laravel project, see https://github.com/laravel/laravel

Using the `.gitlab-ci.yml` file in this directory, the necessary steps are run to build, test and deploy to an
AWS Elastic Container Repository defined in your Gitlab Variables:

- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS_ECR_URI
- AWS_SECRET_ACCESS_KEY

The user for AWS access should need the following policies:

- AmazonEC2ContainerRegistryPowerUser

### Launching with Helm

You will require `kubectl`, a Kubernetes cluster configured and Docker installed (if building locally).

If using this source directory to build your images, make sure you run:
* `composer install`
before building. To build, run:
* `docker build -ti myimagename-nginx -f infrastructure/containers/nginx/Dockerfile .`
* `docker build -ti myimagename-phpfpm -f infrastructure/containers/phpfpm/Dockerfile .`
* `docker build -ti myimagename-artisan -f infrastructure/containers/artisan/Dockerfile .`
You will need these images to be available to your cluster from an accessible repository; the default demo
images built from this source are on Docker Hub. To use your own you will need to configure them in your `production.yaml` (see below).
In normal circumstances, your continuous delivery system will handle getting code to built images and on to
your cluster.

Start your Kubernetes cluster. 

(If using minikube, then `minikube start --extra-config=apiserver.Authorization.Mode=RBAC`)

- If you have not used Helm on this cluster since it was created: `helm init`; if you have installed this Chart (Buckram Helm configuration) before, `helm list` to find the release name and `helm delete RELEASENAME`. Note that `helm init` will trigger a download of the Tiller pod (coordinates Helm actions on the cluster side), and until this has set itself up, Helm will not be ready for use. If necessary, follow the RBAC set-up steps for Helm listed in the Caveats section below
- Copy `infrastructure/config/traefik.yaml.example` to `traefik.yaml` in a safe place and fill in the contents (e.g. the email address for ACME SSL certificates)
- If you have not already, add an ingress controller: `helm install infrastructure/kubernetes/traefik --namespace=kube-system --values ../traefik.yaml --name PLATFORMNAME-ingress`, where `PLATFORMNAME` is, e.g., dev, staging or production (if you wish to have a Traefik dashboard, add `--set dashboard.enabled=true`)
- Copy `infrastructure/config/production.yaml.example` to `production.yaml` in a safe place and fill in the contents (e.g. APP_KEY)
- Make sure the hostname (where webpages should be served, not of the cluster) is correct in both places in `production.yaml`, and, if using minikube, that you have added it to your `/etc/hosts` file with the output of `minikube ip` as the address
- `helm dependencies update infrastructure/kubernetes/buckram`
- `helm install infrastructure/kubernetes/buckram --values PATH_TO_YOUR/production.yaml --name PLATFORMNAME`
- After installing the Traefik ingress, the Route 53 endpoint must be set to point to the load balancer.

At present, the inter-dependencies are not ordered, so you may see, for instance, nginx starting before phpfpm and having to auto-restart before it gets the phpfpm server. Once `kubectl get pods` shows all pods running, your system should be good to go.

If using minikube, rather than a cloud provider, you will have to use a high numbered port to reach the ingress. Run `kubectl get svc --namespace=kube-system` and go to `HOSTNAME:PORT` in your browser, where `HOSTNAME` is the hostname used above, and `PORT` is the 3xxxx numbered port in the line with `traefik` in it (not `traefik-dashboard`)

To help web developers get familiar with Kubernetes, without trying to patch together multiple repositories,
the Helm chart for Kubernetes, container definitions and template secrets are under the `infrastructure`
subdirectory. However, bear in mind that, if you are experimenting
with the Kubernetes workflow by building and pushing images
it is recommended that you do so from a fresh clone of this repository so that
you do not include anything in the `.env` file (or elsewhere in the tree)
that should not end up in the test images. *In particular, make sure your `production.yaml`
is not in the tree*, especially if pushing experimentation images publicly.

(For those unfamiliar with Docker: in normal
workflow, the CI/CD train will clone your code repository
fresh and build the Docker images with your code inside,
then push to your chosen Docker image
repository, from where images may be deployed
to the cluster)

### IAM

To use annotations for IAM:

```
helm install stable/kube2iam --name kube2iam   --set=extraArgs.auto-discover-base-arn=true,extraArgs.auto-discover-default-role=true,host.iptables=true,host.interface=cali+,rbac.create=true
```

### Docker Compose

To test locally, there is a `docker-compose.yml` file within the root of Buckram. This allows a Laravel setup to be tested locally, by copying the `*.env.example` files in `./config/secrets` to the respective `*.env` files and filling the blank fields for testing.

Note that this will create a `storage` folder in the level above the Laravel project above Buckram. This will be used for storing persistent data from `docker-compose` containers, e.g. `postgres` and `artisan`.

### Logging

The elasticsearch chart requires:

```
helm repo add incubator https://kubernetes-charts-incubator.storage.googleapis.com/
```

### Caveats

* A new deployment, if using k8s 1.6.1, kubectl 1.6.2, kops 1.6, should use fixes for helm https://github.com/kubernetes/helm/issues/2224 and https://github.com/containous/traefik/blob/master/examples/k8s/traefik-with-rbac.yaml

[From kujenga's Github comment](https://github.com/kubernetes/helm/issues/2224#issuecomment-299939178)
```
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
```
